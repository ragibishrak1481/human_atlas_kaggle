import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import f1_score

import torch
from torch import nn, optim
from torchvision import models, datasets
from torch.utils import data
from torchvision import transforms

import copy
from utility_classes import Dataset, Threshold
from network_models import*


def main():
    # image path & label data frame
    training_image_path = 'E:/python projects/human atlas/gen_data/train/'
    validation_image_path = 'E:/python projects/human atlas/gen_data/cross_val/'
    train_df = pd.read_csv('E:/python projects/human atlas/train.csv')
    validation_df = pd.read_csv('E:/python projects/human atlas/cross_val.csv')

    # define device to train
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    # device = torch.device('cpu')
    print(device)

    # Hyperparameters
    batch_size = 2
    num_worker = 4
    learning_rate = 0.01
    momentum = 0.9
    epochs = 50
    t_lr = 0.4
    threshold = Threshold(num_classes=28, batch_size=batch_size, lr=t_lr)

    # data preprocessings
    # xform = transforms.Compose([
    #     transforms.ToPILImage(),
    #     transforms.Resize((256, 256)),
    #     transforms.ToTensor()
    # ])
    xform = None
    # load data
    trainset = Dataset(image_path=training_image_path, data_frame=train_df, transform=xform)
    trainloader = data.DataLoader(dataset=trainset,
                                   batch_size=batch_size,
                                   shuffle=True,
                                   num_workers=num_worker)

    validationset = Dataset(image_path=validation_image_path, data_frame=validation_df, transform=xform)
    validationloader = data.DataLoader(dataset=validationset,
                                        batch_size=batch_size,
                                        shuffle=False,
                                        num_workers=num_worker)

    # init model and optimizers
    model = ModelVggBase(28).to(device)
    optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=momentum)
    loss_func = nn.BCEWithLogitsLoss()

    # train
    train_iterations = 1
    report_iterations = 50
    prev_best = 0
    flag = True

    for epoch in range(epochs):

        # training phase
        total_loss = 0
        for _, (image, label) in enumerate(trainloader):
            image = image.to(device)
            label = label.to(device)

            optimizer.zero_grad()

            y = model.forward(image)
            loss = loss_func(y, label)
            loss.backward()
            optimizer.step()
            total_loss += loss.item()

            threshold.update(y.data.detach().cpu().numpy(), label.data.detach().cpu().numpy())
            # print progress
            if train_iterations % report_iterations == 0:
                print("epoch: {} iteration: {} loss: {}".format(epoch, train_iterations, total_loss / report_iterations))
                total_loss = 0
            train_iterations += 1

        # cross validation phase
        total_f1_score = 0
        total_loss = 0
        val_iterations = 0
        model.eval()
        with torch.no_grad():
            for _, (image, label) in enumerate(validationloader):
                image = image.to(device)
                label = label.to(device)

                y = model.forward(image)
                loss = loss_func(y, label)
                total_loss += loss.item()
                val_iterations += 1
                y_ = (torch.sigmoid(y) >= torch.from_numpy(threshold.get()).type(torch.cuda.FloatTensor)).float()
                total_f1_score += f1_score(label.cpu().detach().numpy(), y_.cpu().detach().numpy(), average='macro')
        print("validation loss: {} F1 score: {}".format(total_loss/val_iterations, total_f1_score/(batch_size*val_iterations)))

        model.train()

    # test on the test data


if __name__ == '__main__':
    main()
