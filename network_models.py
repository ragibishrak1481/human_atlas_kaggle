import torch
from torch import nn
import torchvision.models as models


class ModelVggBase(nn.Module):
    def __init__(self, out_classes):
        super(ModelVggBase, self).__init__()
        self.prebase = nn.Sequential(nn.Conv2d(4, 3, 1, stride=1, padding=0, bias=True),
                                     nn.BatchNorm2d(3),
                                     nn.ReLU(inplace=False),
                                     nn.MaxPool2d(2, stride=2))
        self.base = models.vgg11(pretrained=True)
        in_features = self.base.classifier[6].in_features
        self.base.classifier[6] = nn.Linear(in_features, out_classes, bias=True)

    def forward(self, x):
        x = self.prebase(x)
        return self.base(x)


class ModelResnetBase(nn.Module):
    def __init__(self, out_classes):
        super(ModelResnetBase, self).__init__()
        self.prebase = nn.Sequential(nn.Conv2d(4, 3, 1, stride=1, padding=0, bias=True),
                                     nn.BatchNorm2d(3),
                                     nn.ReLU(inplace=False),
                                     nn.MaxPool2d(2, stride=2))

        self.base = models.resnet18(pretrained=True)
        # in_features = self.base.fc.in_features
        self.base.fc = nn.Linear(2048, out_classes, bias=True)

    def forward(self, x):
        x = self.prebase(x)
        x = self.base(x)
        return x
