import numpy as np
import torch
from torch import nn, optim
from torchvision import models, datasets
from torch.utils import data


class Dataset(data.Dataset):
    ''' Loads images saved as .pt files.
    First channel (green) is excluded.
    No need to transform to tensor'''
    def __init__(self, image_path, data_frame, transform=None):
        super().__init__()
        self.image_path = image_path
        self.df = data_frame
        self.transform = transform

    def __getitem__(self, index):
        X = torch.load(self.image_path + self.df.loc[index, 'Id'] + '.pt')
        X = X[:, :, :].permute(2, 0, 1).type(torch.FloatTensor)
        y = torch.from_numpy(self.df.iloc[index, 2:].values.astype(np.float32))

        if self.transform is not None:
            X = self.transform(X)

        return X, y

    def __len__(self):
        return self.df.shape[0]


class Threshold:
    def __init__(self, num_classes, batch_size, lr):
        self.lr = lr
        self.threshold = None
        self.flag = True
        self.num_classes = num_classes

    def get(self):
        return self.threshold

    def update(self, pred, label):

        if self.flag:
            self.flag = False
            self.threshold = (1 / (1 + np.exp(-1 * pred))) + \
                             np.random.uniform(0, .02, (1, self.num_classes)) - 0.1
            return

        pred_bin = (1 / (1 + np.exp(-1 * pred)) >= self.threshold).astype(np.float32)
        self.threshold = (1-self.lr)*self.threshold + \
                         self.lr * np.average((pred_bin-label)*np.abs(pred-self.threshold), axis=0)
        return
